<?php

require_once(__DIR__ . '/util.php');
//2つのクラス
require_once('Quiz.php');
require_once('Token.php');

//正解を取得する
$quiz = new MyApp\Quiz();

// 例外処理
// ポストでうまく解答を渡せなかった場合
try {
  $correctAnswer = $quiz->checkAnswer();
} catch (Exception $e) {
  header($_SERVER['SERVER_PROTOCOL'] . ' 403 Forbidden', true, 403);
  echo $e->getMessage();
  exit;
}

//JSONで返す
//JSONを返す場合のヘッダー
header('Content-Type: application/json; charset=UTF-8');
//JSONで正しい答えを返す
echo json_encode([
  'correct_answer' => $correctAnswer
]);
