<?php
//現在のディレクトリと同じ場所のファイルを読み込む
//require_once(__DIR__ . '/config.php');

//
require_once('util.php');
//2つのクラス
require_once('Quiz.php');
require_once('Token.php');

$quiz = new MyApp\Quiz();

if (!$quiz->isFinished()) {
  $data = $quiz->getCurrentQuiz();
  shuffle($data['a']);
}

?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>クイズ | 首都はどこ？</title>
  <link rel="stylesheet" href="styles.css">
</head>
<body>
  <div class="pageTop_wrapper">    
    <h1 class="pageTop_title">首都はどこ？？</h1>
    <p>以下の問題を読んで、答えを選んでね！</p>
  </div>
  <!-- 最終問題の場合 -->
  <?php if ($quiz->isFinished()) : ?>
    <div id="container">
      <!-- 結果を表示する -->
      <div id="result">
        あなたの正答率は ...
        <div><?= h($quiz->getScore()); ?> %</div>
      </div>
      <!-- リプレイボタン -->
      <a href=""><div id="btn">もう一回?</div></a>
    </div>
    <!-- 問題をリセットする -->
    <?php $quiz->reset(); ?>

  <!-- 最終問題以外の場合 -->
  <?php else : ?>
    <div id="container">
      <!-- h1タグに問題文を表示 -->
      <h1>Q<?= h($_SESSION['current_num'] + 1);?>. <?= h($data['q']); ?></h1>
      <!-- foreach文で選択肢リストを表示 -->
      <ul>
        <?php foreach ($data['a'] as $a) : ?>
          <li class="answer"><?= h($a); ?></li>
        <?php endforeach; ?>
      </ul>
      <!-- ボタンの処理 -->
      <div id="btn" class="disabled">
        <!-- 三項演算子 最終問題？true:結果を表示 /false 次の問題へ -->
        <?= $quiz->isLast() ? '結果を見る' : '次の質問へ'; ?>
      </div>
      <!-- トークンでCSRF対策 -->
      <input type="hidden" id="token" value="<?= h($_SESSION['token']); ?>">
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="quiz.js"></script>
  <?php endif; ?>
</body>
</html>
