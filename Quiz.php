<?php

namespace MyApp;

class Quiz {
  //質問文と解答を配列に$_quizSet入れる
  //プライベートプロパティ
  private $_quizSet = [];
  // コンストラクタ
  public function __construct() {
    $this->_setup();
    //staticメソッド
    // トークンをセットする
    Token::create();

    //もしセッションの現在の問題番号が取得されていなければ
    if (!isset($_SESSION['current_num'])) {
      $this->_initSession();
    }
  }
  //プライベートファンクション
  private function _initSession() {
    //問題番号
    $_SESSION['current_num'] = 0;
    //正答数
    $_SESSION['correct_count'] = 0;
  }

  //答え合わせメソッド
  public function checkAnswer() {

    Token::validate('token');
    //解答をAJAXのJASONより引っ張ってくる。
    //正解は現在出ている配列番号の[0]番名
    $correctAnswer = $this->_quizSet[$_SESSION['current_num']]['a'][0];

    //正解が設定されていない場合、例外処理でエラーを投げる
    if (!isset($_POST['answer'])) {
      throw new \Exception('answer not set!');
    }
    // 正答数をカウントしセッションに入れる
    if ($correctAnswer === $_POST['answer']) {
      $_SESSION['correct_count']++;
    }
    //問題番号を増やす
    $_SESSION['current_num']++;
    //
    return $correctAnswer;
  }
  //最終問題での処理メソッド
  public function isFinished() {
    return count($this->_quizSet) === $_SESSION['current_num'];
  }
  //正答率を計算するメソッド
  public function getScore() {
    return round($_SESSION['correct_count'] / count($this->_quizSet) * 100);
  }

  //最終問題かどうか確認
  //index.phpのボタン処理に必要
  // 通常：次の問題　最終問題：結果を表示
  public function isLast() {
    //プラス１を足す
    return count($this->_quizSet) === $_SESSION['current_num'] + 1;
  }

  //current_numをリセットする関数
  public function reset() {
    $this->_initSession();
  }

  //現在表示されるクイズを取得する
  public function getCurrentQuiz() {
    return $this->_quizSet[$_SESSION['current_num']];
  }

  //問題文と選択肢
  //[0]が正解
  private function _setup() {
    $this->_quizSet[] = [
      'q' => '日本の首都は?',
      'a' => ['東京', '京都', '大阪']
    ];
    $this->_quizSet[] = [
      'q' => 'オーストラリアの首都は?',
      'a' => ['キャンベラ', 'シドニー', 'メルボルン', 'ブリスベン']
    ];
    $this->_quizSet[] = [
      'q' => 'アメリカの首都は',
      'a' => ['ワシントンDC', 'ニューヨーク', 'ロサンゼルス', 'フィラデルフィア']
    ];
    $this->_quizSet[] = [
      'q' => 'カナダの首都は',
      'a' => ['オタワ', 'バンクーバー', 'トロント', 'モントリオール']
    ];
    $this->_quizSet[] = [
      'q' => '中国の首都は',
      'a' => ['北京', '上海', '重慶', '成都','香港']
    ];
    $this->_quizSet[] = [
      'q' => 'タイの首都は',
      'a' => ['バンコク', 'プーケット', 'チェンマイ', 'アユタヤ']
    ];
    $this->_quizSet[] = [
      'q' => 'オーストリアの首都は',
      'a' => ['ウィーン', 'プラハ', 'ザルツブルク', 'ブタペスト', 'ベルリン']
    ];
    $this->_quizSet[] = [
      'q' => 'エジプトの首都は',
      'a' => ['カイロ', 'アレクサンドリア', 'ルクソール']
    ];

  }
}
