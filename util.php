<?php
session_start();
//エラー表示の設定
ini_set('display_errors', 1);

//エスケープ関数
function h($s) {
  return htmlspecialchars($s, ENT_QUOTES, 'UTF-8');
}
