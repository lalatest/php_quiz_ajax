<?php
//CSRF処理
namespace MyApp;
//変なフォームから送られてきた時の処理
class Token {
//トークンの設定
  static public function create() {
    // トークンが入っていない場合
    if (!isset($_SESSION['token'])) {
      // 32桁の推測されにくい数字を設定
      $_SESSION['token'] = bin2hex(openssl_random_pseudo_bytes(16));
    }
  }
// トークンが入っているキーをパラメーターで渡す
// おかしな場合は例外を返す

  static public function validate($tokenKey) {
    if (
      // セッションがセットされていなかった場合
      !isset($_SESSION['token']) ||
      // ポストされたキーがない場合
      !isset($_POST[$tokenKey]) ||
      // 上記値が一致していない場合
      $_SESSION['token'] !== $_POST[$tokenKey]
    ) {
      throw new \Exception('invalid token!');
    }
  }
}
