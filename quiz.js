$(function() {
  //実行
  'use strict';

  //回答(index.php ul li .answerクラス)がクリックされた場合の関数
  $('.answer').on('click', function() {
    //変数宣言 $selected ＝選択された回答
    var $selected = $(this);

    //選択肢を一度選んだら、他の回答をできなくする
    //回答がcorrectクラスまたはwrongクラスを持っている場合
    if ($selected.hasClass('correct') || $selected.hasClass('wrong')) {
      //returnで他の選択肢を選べないようにする
      return;
    }

    //選択された回答$selectedにselectedクラスを追加
    $selected.addClass('selected');
    //変数宣言 $answer ＝選択肢の文字列
    var answer = $selected.text();

    //答えを読み込む
    //ajax処理
    $.post('answer.php', {
      // ajaxで答えを渡す
      // answerという名前で上記変数answerを渡す
      //トークンの値
      answer: answer,
      token: $('#token').val()
      //終わった時の処理
    }).done(function(res) {
      //answerクラスにすべて付ける
      $('.answer').each(function() {
        //現在選択された答えの文字列が、正解と同じか
        if ($(this).text() === res.correct_answer) {
          //正解の時はcorrectクラスをつけてCSSでライムグリーン太字に
          $(this).addClass('correct');
        } else {
          //不正解の時はwrongクラスをつけてグレーに
          $(this).addClass('wrong');
        }
      });

      //解答を表示する
      //解答が読み込めているかアラートで確認
      // alert(res.correct_answer);
      //条件分岐（回答＝解答）、表示する文字列を上書き
      if (answer === res.correct_answer) {
        // 正解の場合
        $selected.text(answer + ' ... 正解!');
      } else {
        // 間違っている場合
        $selected.text(answer + ' ... 間違い!');
      }
      $('#btn').removeClass('disabled');
    });
  });

  //ボタンをクリックすると次の問題に移る関数
  $('#btn').on('click', function() {
    //選択肢のクラスがdisabledの場合＝未回答
    //disabledクラスをもっていない＝回答済
    if (!$(this).hasClass('disabled')) {
      location.reload();
    }
  });

});
